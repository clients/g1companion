import { defineConfig } from 'vite'
import { r } from './scripts/utils'
import { sharedConfig } from './vite.config'

// bundling the content script using Vite
export default defineConfig({
  ...sharedConfig,
  root: r('src/site'),
  plugins: [
    ...sharedConfig.plugins!,
  ],
})
