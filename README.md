# Ğ1Companion web extension

## Summary

Web extension based on [vitesse-webext](https://github.com/antfu/vitesse-webext) starter template with :

- [anu UI](https://anu-vue.netlify.app/) - DX focused utility based Vue component library built on top of UnoCSS & VueUse. [Repo](https://github.com/jd-solanki/anu).
- [unoCSS](https://uno.antfu.me/) - the instant on-demand Atomic CSS engine.
- [VueUse](https://github.com/antfu/vueuse) - collection of useful composition APIs
- [Icones.js.org](https://icones.js.org) - use icons from any icon sets
- [`webext-bridge`](https://github.com/antfu/webext-bridge) - effortlessly communication between contexts (background, popup, contentScripts)
- [Web extension architecture](https://developer.chrome.com/docs/extensions/mv2/architecture-overview/#arch) - Chrome extensions documentation
- [g1lib.js](https://git.duniter.org/libs/g1lib.js) - An ubiquitous static javascript lib for Ǧ1 / Duniter ecosystem with reliability in mind.
- [vue i18n](https://vue-i18n.intlify.dev/) - Internationalization plugin for Vue.js

## Installation

1. Clonez le dépôt :

    `git clone https://git.duniter.org/clients/g1companion`

2. Allez dans le répertoire `g1companion`, puis installez les packages :

    `pnpm i`

3. Démarrez l'extension en mode développement :

    `pnpm dev`

    Ou compilez l'extension pour tester seulement :

    `pnpm build`

Une fois l'extension lancée en mode dev ou compilée, il faut l'installer dans firefox ou Chrome.

## Avec Firefox

1. Allez dans les thèmes et extensions (`Ctrl+Shift+A` ou `about:addons`)
2. Cliquez sur la roue à droite de "Gérer vos extensions" et sélectionnez "Déboguer des modules" (ou `about:debugging#/runtime/this-firefox`)
3. Cliquez sur "Charger un module complémentaire temporaire..."
4. Choisissez le fichier `extension/manifest.json` et l'extension devrait être installée ! (ex: `~/dev/g1companion/extension/manifest.json`)
5. En mode dev, cliquez sur "Examiner" pour ouvrir la console devtools. ⚡️ La console est celle du `background`.
6. Ouvrez la popup. Sélectionnez "désactiver le masquage automatique" dans le menu ••• en haut à droite dans la console
7. Pour choisir la console de la popup, une fois la popup ouverte, cliquez sur l'îcone "sélectionner un iframe" (située à gauche du menu •••) pour choisir le fichier `/dist/popup/index.html`.

Vous pouvez aussi ouvrir la popup, puis clic-droit sur un lien et "Ouvrir dans un nouvel onglet".
Sinon, copiez le UUID interne et ouvrez un onglet sur `moz-extension://UUID/dist/popup/index.html`.

## Avec Chrome

1. Allez dans la page de gestion des extensions chrome://extensions/
2. Activez le mode développeur (même si vous ne souhaitez pas développer. L'extension n'est pas encore sur les stores)
3. Cliquez sur "Charger l'extension non empaquetée"
4. Sélectionnez le dossier `extension` directement et ça devrait être installé !

Pour le mode dev, un clic droit dans la popup ou la page option permet d'accéder aux devtools. Pour le background, cliquez sur le lien "Examiner les vues" dans la page des extensions.

## En mode dev, chargez automatiquement vos wallets

Ajoutez un fichier `.walletsV1.json` à la racine du dépôt avec comme données vos wallets :

```json
[
  {
    "title": "ManUtopiK",
    "pubkey": "2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8",
    "id": "MY_ID",
    "password": "MY_PASSWORD",
    "default": true
  }
]
```

Vous pouvez ajouter plusieurs wallets. Le paramètre `"default": true` active le wallet.

Pour tester l'extension, vous pouvez ouvrir le fichier `test-api.html` avec les boutons de test dans votre navigateur.

# Knowledges base and inspiration

https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions
https://developer.chrome.com/docs/extensions/

https://github.com/mdn/webextensions-examples
https://github.com/GoogleChrome/chrome-extensions-samples



# README WebExtension Vite Starter

Last update on commit : [6ced6dc42b344d1b1e82bc42b4a3d6c261242f97](https://github.com/antfu/vitesse-webext/commit/6ced6dc42b344d1b1e82bc42b4a3d6c261242f97)

<details>
<summary>Original below</summary>

# WebExtension Vite Starter
https://github.com/antfu/vitesse-webext

A [Vite](https://vitejs.dev/) powered WebExtension ([Chrome](https://developer.chrome.com/docs/extensions/reference/), [FireFox](https://addons.mozilla.org/en-US/developers/), etc.) starter template.

<p align="center">
<sub>Popup</sub><br/>
<img width="655" src="https://user-images.githubusercontent.com/11247099/126741643-813b3773-17ff-4281-9737-f319e00feddc.png"><br/>
<sub>Options Page</sub><br/>
<img width="655" src="https://user-images.githubusercontent.com/11247099/126741653-43125b62-6578-4452-83a7-bee19be2eaa2.png"><br/>
<sub>Inject Vue App into the Content Script</sub><br/>
<img src="https://user-images.githubusercontent.com/11247099/130695439-52418cf0-e186-4085-8e19-23fe808a274e.png">
</p>

## Features

- ⚡️ **Instant HMR** - use **Vite** on dev (no more refresh!)
- 🥝 Vue 3 - Composition API, [`<script setup>` syntax](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0040-script-setup.md) and more!
- 💬 Effortless communications - powered by [`webext-bridge`](https://github.com/antfu/webext-bridge) and [VueUse](https://github.com/antfu/vueuse) storage
- 🌈 [UnoCSS](https://github.com/unocss/unocss) - The instant on-demand Atomic CSS engine.
- 🦾 [TypeScript](https://www.typescriptlang.org/) - type safe
- 📦 [Components auto importing](./src/components)
- 🌟 [Icons](./src/components) - Access to icons from any iconset directly
- 🖥 Content Script - Use Vue even in content script
- 🌍 WebExtension - isomorphic extension for Chrome, Firefox, and others
- 📃 Dynamic `manifest.json` with full type support

## Pre-packed

### WebExtension Libraries

- [`webextension-polyfill`](https://github.com/mozilla/webextension-polyfill) - WebExtension browser API Polyfill with types
- [`webext-bridge`](https://github.com/antfu/webext-bridge) - effortlessly communication between contexts

### Vite Plugins

- [`unplugin-auto-import`](https://github.com/antfu/unplugin-auto-import) - Directly use `browser` and Vue Composition API without importing
- [`unplugin-vue-components`](https://github.com/antfu/vite-plugin-components) - components auto import
- [`unplugin-icons`](https://github.com/antfu/unplugin-icons) - icons as components
- [Iconify](https://iconify.design) - use icons from any icon sets [🔍Icônes](https://icones.netlify.app/)

### Vue Plugins

- [VueUse](https://github.com/antfu/vueuse) - collection of useful composition APIs

### UI Frameworks

- [unoCSS](https://github.com/unocss/unocss) - the instant on-demand Atomic CSS engine.

### Coding Style

- Use Composition API with [`<script setup>` SFC syntax](https://github.com/vuejs/rfcs/pull/227)
- [ESLint](https://eslint.org/) with [@antfu/eslint-config](https://github.com/antfu/eslint-config), single quotes, no semi

### Dev tools

- [TypeScript](https://www.typescriptlang.org/)
- [pnpm](https://pnpm.js.org/) - fast, disk space efficient package manager
- [esno](https://github.com/antfu/esno) - TypeScript / ESNext node runtime powered by esbuild
- [npm-run-all](https://github.com/mysticatea/npm-run-all) - Run multiple npm-scripts in parallel or sequential
- [web-ext](https://github.com/mozilla/web-ext) - Streamlined experience for developing web extensions

## Use the Template

### GitHub Template

[Create a repo from this template on GitHub](https://github.com/antfu/vitesse-webext/generate).

### Clone to local

If you prefer to do it manually with the cleaner git history

> If you don't have pnpm installed, run: npm install -g pnpm

```bash
npx degit antfu/vitesse-webext my-webext
cd my-webext
pnpm i
```

## Usage

### Folders

- `src` - main source.
  - `contentScript` - scripts and components to be injected as `content_script`
  - `background` - scripts for background.
  - `components` - auto-imported Vue components that are shared in popup and options page.
  - `styles` - styles shared in popup and options page
  - `assets` - assets used in Vue components
  - `manifest.ts` - manifest for the extension.
- `extension` - extension package root.
  - `assets` - static assets (mainly for `manifest.json`).
  - `dist` - built files, also serve stub entry for Vite on development.
- `scripts` - development and bundling helper scripts.

### Development

```bash
pnpm dev
```

Then **load extension in browser with the `extension/` folder**.

For Firefox developers, you can run the following command instead:

```bash
pnpm start:firefox
```

`web-ext` auto reload the extension when `extension/` files changed.

> While Vite handles HMR automatically in the most of the case, [Extensions Reloader](https://chrome.google.com/webstore/detail/fimgfedafeadlieiabdeeaodndnlbhid) is still recommanded for cleaner hard reloading.

### Build

To build the extension, run

```bash
pnpm build
```

And then pack files under `extension`, you can upload `extension.crx` or `extension.xpi` to appropriate extension store.

## Credits

[![Volta](https://user-images.githubusercontent.com/904724/195351818-9e826ea9-12a0-4b06-8274-352743cd2047.png)](https://volta.net)

This template is originally made for the [volta.net](https://volta.net) browser extension.

## Variations

This is a variant of [Vitesse](https://github.com/antfu/vitesse), check out the [full variations list](https://github.com/antfu/vitesse#variations).
</details>