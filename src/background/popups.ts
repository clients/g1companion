import { isInternalEndpoint, onMessage } from 'webext-bridge'
import type { IBridgeMessage } from 'webext-bridge'

import { setBadge } from './badge'

// Set badge to alert user that popup is open
function setAlertBadge(text = '!') {
  browser.browserAction.setBadgeBackgroundColor({ color: 'red' })
  setBadge(text)
}

interface popupOptions {
  path: string
  tabId: number
  params: URLSearchParams
  height?: number
}

async function openPopup({ path, params, tabId, height = 400 }: popupOptions) {
  // TODO: prevent opening popup multiple times. See example code here : https://stackoverflow.com/a/68456858/479957

  // Default popup size
  const width = 500

  const tabInfo = await browser.tabs.get(tabId)

  const tabWindow = await browser.windows.get(tabInfo.windowId!)

  // const height = Math.round(tabWindow.height * 0.75) // dynamic height
  const top = Math.round((tabWindow.height! - height) / 2 + tabWindow.top!)
  const left = Math.round((tabWindow.width! - width) / 2 + tabWindow.left!)

  browser.windows.create({
    url: `/dist/popup/index.html#/browserPopup${path}?${params}`,
    type: 'popup',
    width,
    height,
    left,
    top,
    // titlePreface: 'g1companion', // don't work with browser polyfill
  })
}

function initPopups() {
  onMessage('pay-popup', async ({ data, sender }: IBridgeMessage<{}>) => {
    if (isInternalEndpoint(sender) && data) {
      setAlertBadge()

      const params = new URLSearchParams(data)
      await openPopup({
        path: '/pay',
        params,
        tabId: sender.tabId,
        height: 520,
      })
    }
  })

  onMessage('login-popup', async ({ data, sender }: IBridgeMessage<{ url: string }>) => {
    if (isInternalEndpoint(sender) && data) {
      setAlertBadge()

      const screenshot = ''
      // TODO doesn't work in chrome browser ?
      // screenshot = await browser.tabs.captureTab(sender.tabId, {
      //   format: 'jpeg',
      //   quality: 60,
      //   scale: 0.5,
      // })

      const params = new URLSearchParams({ ...data, tabId: sender.tabId as unknown as string, screenshot })
      await openPopup({
        path: '/login',
        params,
        tabId: sender.tabId,
        height: 520,
      })
    }
  })

  onMessage('close-popup', () => {
    setBadge('')
    browser.browserAction.setBadgeBackgroundColor({ color: 'rgba(37, 99, 235)' })
  })
}

export { initPopups }
