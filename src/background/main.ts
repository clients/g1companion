import { isInternalEndpoint, onMessage, sendMessage } from 'webext-bridge'
import type { Tabs } from 'webextension-polyfill'

import { initBadge } from './badge'
import { initPopups } from './popups'
import { connectWebSocket } from './webSocket'

import storage, { currentWallet, storageIdentities } from '~/logic/storage/extension'
import { useOnStorageChange } from '~/composables/useStorage'

initBadge()
initPopups()

// Subscribe to global storage change
useOnStorageChange(storage)

// Initialize websocket and watch for currentWallet change
watch(currentWallet, ({ pubkey }) => {
  connectWebSocket(pubkey)
})

// load G1 identities
async function loadIdentities() {
  // TODO: store storageIdentities.value.time response and prevent refetch less than 3 hours. @poka config
  const response = await fetch('https://g1-stats.axiom-team.fr/data/geoloc-members.json')
  storageIdentities.value = (await response.json()).wallets
  console.log('loadedIdentities', storageIdentities.value)
}
loadIdentities()

// only on dev mode
if (import.meta.hot) {
  // @ts-expect-error for background HMR
  import('/@vite/client')
  // load latest content script
  import('./contentScriptHMR')
}

browser.runtime.onInstalled.addListener((): void => {
  // TODO: check update ?
  console.log('Extension installed')
})

let previousTabId = 0

// communication example: send previous tab title from background page
// see shim.d.ts for type declaration
browser.tabs.onActivated.addListener(async ({ tabId }) => {
  if (!previousTabId) {
    previousTabId = tabId
    return
  }

  let tab: Tabs.Tab

  try {
    tab = await browser.tabs.get(previousTabId)
    previousTabId = tabId
  }
  catch {
    return
  }

  console.log('previous tab', tab)
  sendMessage(
    'tab-prev',
    { title: tab.title },
    { context: 'content-script', tabId },
  )
})

onMessage('get-current-tab', async () => {
  try {
    const tab = await browser.tabs.get(previousTabId)
    return {
      title: tab?.title,
    }
  }
  catch {
    return {
      title: undefined,
    }
  }
})

onMessage('fetch', async ({ data, sender }) => {
  // Respond only if request is from 'devtools', 'content-script' or 'background' endpoint
  if (isInternalEndpoint(sender) && data) {
    const response = await fetch(data)
    return response.json()
  }
})

