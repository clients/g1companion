import { incrementBadge } from './badge'

let wsConnexion: WebSocket | null = null

// TODO: use usewebsocket https://vueuse.org/core/usewebsocket/#usewebsocket
export function connectWebSocket(pubkey: string) {
  if (pubkey) {
    if (wsConnexion) {
      console.log(`Force close ${wsConnexion.url}`)
      wsConnexion.close()
    }

    // TODO: use cesiumPlusUrl
    wsConnexion = new WebSocket(
      `wss://g1.data.e-is.pro/ws/event/user/${pubkey}/fr-FR`,
    )

    wsConnexion.onmessage = function (event) {
      // TODO: handle message event
      console.log(`ws:onmessage ${pubkey}`, event)
      incrementBadge()
    }

    wsConnexion.onopen = function (event) {
      console.log(`ws:onopen ${pubkey}`, event)
    }

    wsConnexion.onclose = function (event: CloseEvent) {
      console.log(`ws:onclose ${pubkey}`)
    }
  }
}
