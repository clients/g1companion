import { onMessage } from 'webext-bridge'
import { createApp } from 'vue'
import App from './views/App.vue'
import '~/styles'

// Firefox `browser.tabs.executeScript()` requires scripts return a primitive value
(() => {
  if (__DEV__)
    console.info('[Ğ1Companion] Hello dev !')

  // communication example: send previous tab title from background page
  onMessage('tab-prev', ({ data }) => {
    console.log(`[Ğ1Companion] Navigate from page "${data.title}"`)
  })

  // mount component to context window
  const container = document.createElement('div')
  const root = document.createElement('div')
  const styleEl = document.createElement('link')
  const shadowDOM = container.attachShadow?.({ mode: __DEV__ ? 'open' : 'closed' }) || container
  styleEl.setAttribute('rel', 'stylesheet')
  styleEl.setAttribute('href', browser.runtime.getURL('dist/contentScripts/style.css'))
  shadowDOM.appendChild(styleEl)
  shadowDOM.appendChild(root)
  document.body.appendChild(container)
  createApp(App).mount(root)

  /**
   * Content scripts are executed in an "isolated world" environment.
   * See https://stackoverflow.com/questions/9515704/use-a-content-script-to-access-the-page-context-variables-and-functions/9517879#9517879
   */
  const s: HTMLScriptElement = document.createElement('script')
  s.src = browser.runtime.getURL('dist/contentScripts/site-api.js')
  s.onload = function () {
    this.remove()
  };
  (document.head || document.documentElement).appendChild(s)
})()
