import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import App from './Popup.vue'
import routes from '~pages'
import i18n from '~/logic/i18n'
import '~/styles'

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (to.hash)
          return resolve({ selector: to.hash })

        else if (savedPosition)
          return resolve(savedPosition)

        else
          resolve(document.getElementById('app').scrollIntoView({ behavior: 'smooth' }))
      }, 110)
    })
  },
})

const app = createApp(App)
  .use(router)
  .use(i18n)

// app.config.globalProperties.$dayjs = dayjs

// Performance Tracing with vue devtools
// app.config.performance = true
app.mount('#app')
