declare const __DEV__: boolean

interface Wallet {
  title?: string
  pubkey: string
  secretKey?: string
}

interface Identity {
  title: string
  pubkey: string
}
