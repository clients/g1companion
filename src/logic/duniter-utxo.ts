import { storageUtxos } from '~/logic/storage/extension'

/**
 * Select utxos needed for transaction
 * @param  {Array} _response	fetched sources containing utxos
 * @param  {Integer} _amount 	defines how many utxos are needed
 * @param  {String} _pubkey 	sender's public key
 * @return {Array}      		utxos array after selection
 */
export function selectUtxo(_response: any, _amount: number, _pubkey: string) {
  const array_utxo: any = []

  _response.sources.forEach((source: any) => {
    array_utxo.push(source)
  })

  const result = filterUtxo(array_utxo, _amount, `SIG(${_pubkey})`)

  return result
}

/**
 * Creates new utxos array from start index to end index
 * @param  {Array} _arrayUtxo 	utxos array to be sliced
 * @param  {Integer} start 		defines start index
 * @param  {Integer} end 		defines end index
 * @return {Array}      		utxos array after slice
 */
export function slice_utxo(_arrayUtxo: [string], start: number, end: number) {
  let new_array = []
  new_array = _arrayUtxo.slice(start, end)
  return new_array
}

/**
 * Creates a new utxo
 * @param  {Array} _type		new utxo type
 * @param  {Array} _noffset 	new utxo noffset
 * @param  {Array} _identifier 	new utxo identifier
 * @param  {Array} _amount 		new utxo amount
 * @param  {Array} _base 		new utxo base
 * @param  {Array} _conditions 	new utxo condition
 * @return {Array}      		new utxo created
 */
export function createNewUtxo(
  _type,
  _noffset,
  _identifier,
  _amount,
  _base,
  _conditions,
) {
  const newUtxo = {
    type: _type,
    noffset: _noffset,
    identifier: _identifier,
    amount: _amount,
    base: _base,
    conditions: _conditions,
  }

  return newUtxo
}

/**
 * Removes utxos array from another utxos array
 * @param  {Array} _arrayUtxo1	first utxos array to remove
 * @param  {Array} _arrayUtxo2 	second utxos array
 * @return {Array}      		utxos array after first utxos array has been removed from second utxos array
 */
export function removeUtxosFromArray(_arrayUtxo1, _arrayUtxo2) {
  _arrayUtxo1.forEach((_utxo) => {
    _arrayUtxo2 = removeUtxoFromArray(_utxo, _arrayUtxo2)
  })
  return _arrayUtxo2
}

/**
 * Removes utxo item from utxos array
 * @param  {Array} _utxo 		utxo item to remove
 * @param  {Array} _arrayUtxo 	utxos array
 * @return {Array}      		utxos array after utxo item has been removed
 */
function removeUtxoFromArray(_utxo, _arrayUtxo) {
  let index = 0
  _arrayUtxo.forEach((currentUtxo) => {
    if (
      currentUtxo.noffset === _utxo.noffset
        && currentUtxo.identifier === _utxo.identifier
    ) {
      _arrayUtxo.splice(index, 1)
      index++
    }
  })
  return _arrayUtxo
}

/**
 * Stores used utxos to localstorage
 * @param  {Array} _utxos	utxos array
 */
export function storeUsedUtxo(_utxos) {
  _utxos.forEach((_utxo) => {
    if (_utxo !== '0') {
      const stored = isUtxoStored(_utxo)

      if (!stored) {
        const d = new Date()
        const newUtxoTimestampObject = { timestamp: d, data: _utxo }
        storageUtxos.value.push(newUtxoTimestampObject)
      }
    }
    else {
      console.log('storedUsedUtxo error : _utxo undefined')
    }
  })
}

/**
 * Sums up amounts in utxos array
 * @param  {Array} _arrayUtxo	utxos array
 * @return {Integer} 			summed amount
 */
export function getTotalUtxoAmount(_arrayUtxo) {
  let utxoTotalAmount = 0

  _arrayUtxo.forEach((_utxo) => {
    utxoTotalAmount = utxoTotalAmount + _utxo.amount
  })
  return utxoTotalAmount
}

/**
   * Filters utxos array to keep utxos needed for the transaction with certain condition ("SIG(pubkey"))
   * @param  {Array} _arrayUtxo 	array filled with utxos
   * @param  {Integer} _amount 	defines how many utxos are needed
   * @param  {String} condition 	condition that all utxos have to fill
   * @return {Array}      		utxos array after filtration
   */
function filterUtxo(_arrayUtxo, _amount, condition) {
  const utxoFiltered = []
  let utxoTotal = 0
  let index = 0

  while (utxoTotal < _amount) {
    index++
    if (condition === _arrayUtxo[index].conditions) {
      if (!isUtxoStored(_arrayUtxo[index])) {
        utxoTotal = utxoTotal + _arrayUtxo[index].amount
        utxoFiltered.push(_arrayUtxo[index])
      }
    }
  }
  return utxoFiltered
}

/**
   * Checks if a utxo is stored in local storage & erase utxos stored more than 1h ago from local storage
   * @param  {Array} _utxo	utxo to check
   * @return {Boolean} 		says if utxo is stored in local storage
   */
function isUtxoStored(_utxo) {
  let count = 0
  let stored = true
  const d = new Date()
  let index = 0
  storageUtxos.value.forEach((storageUtxo) => {
    const storeDate = new Date(storageUtxo.timestamp)

    if (storeDate.getTime() + 3600000 < d.getTime())
    // empties storageUtxos if timestamp > 1h
      storageUtxos.value.splice(index, 1)
    else if (
      _utxo.noffset === storageUtxo.data.noffset
        && _utxo.identifier === storageUtxo.data.identifier
    ) {
      count++
    }
    index++
  })

  if (count === 0)
    stored = false

  return stored
}
