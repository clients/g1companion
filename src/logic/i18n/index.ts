import { createI18n } from 'vue-i18n'
import messages from '@intlify/vite-plugin-vue-i18n/messages'

import { userLocale } from '~/logic/storage/extension'

// TODO user locale not set when popup opened
const options = {
  legacy: false, // you must set `false`, to use Composition API
  locale: userLocale.value || navigator.language.split('-')[0], // set locale
  fallbackLocale: 'en', // set fallback locale
  messages, // set locale messages from ~/locales folder
}
const i18n = createI18n(options)

// @ts-expect-error Type instantiation is excessively deep and possibly infinite.ts(2589)
const { locale, t, tc, te, d, n } = i18n.global
export { locale, t, tc, te, d, n }

export default i18n
