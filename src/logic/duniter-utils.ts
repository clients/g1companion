/**
 * Converts amount from String into Integer then multiplies by 100 (=> base 0)
 * @param  {String} _amount amount value before conversion
 * @return {Integer} amount value after conversion
 */
export function convertToInteger(_amount: any) {
  _amount = _amount.replace(/,/, '.')
  _amount = parseFloat(_amount) * 100
  _amount = _amount.toFixed()
  return _amount
}

/**
   * Checks if comment has any special characters to remove or adds "." if comment field is empty
   * @param  {String} _comment comment value before checking
   * @return {String} comment value after checking
   */
export function checkSpecialChar(_comment: string) {
  _comment = _comment.normalize('NFD').replace(/[\u0300-\u036F]/g, '')
  _comment = _comment.replace(/[^a-zA-Z0-9,;:/\-.!? ]/g, ' ')
  if (!_comment)
    _comment = '.'
  return _comment
}
