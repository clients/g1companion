import * as g1lib from 'g1lib'

import { createNewUtxo, getTotalUtxoAmount, removeUtxosFromArray, selectUtxo, slice_utxo, storeUsedUtxo } from './duniter-utxo'
import { checkSpecialChar, convertToInteger } from './duniter-utils'
import { duniterUrl, storageWallets } from '~/logic/storage/extension'

const MAX_INPUT_LENGTH = 40

interface Tx {
  from: string
  to: string
  amount: number
  comment: string
}

export async function sendTransaction({ from, to, amount, comment }: Tx) {
  const sources = await fetchResponse(`/tx/sources/${from}`, duniterUrl.value)
  const currentBlock = await fetchResponse('/blockchain/current', duniterUrl.value)
  const formatedAmount = convertToInteger(amount)

  //   utxoValue => all utxos needed for the transaction
  const utxoValue = selectUtxo(sources, formatedAmount, from)

  const result = await createAndSendTransaction(
    currentBlock,
    utxoValue,
    formatedAmount,
    getCurrentWallet(from),
    to,
    from,
    checkSpecialChar(comment),
  )
  console.log(result)
}

/**
 * Fetches data from url
 * @param  {String} _cmd command to complete url - ex : _cmd -> '/tx/process'
 * @param  {String} _url duniter url - ex : _url -> 'http://93.8.54.71:20901'
 * @return {String}      data fetched from url content
 */
async function fetchResponse(_cmd: string, _url: string) {
  const url = `${_url}${_cmd}`
  const response: any = await fetch(url).then(res => res.json())
  // detailsMsg2.value += `--> Valeur de l'url ${url}`
  return response
}

/**
   * Gets sender's current wallet
   * @param  {String} _pubkey sender's public key
   * @return {Wallet} sender's wallet linked to public key
   */
function getCurrentWallet(_pubkey: string) {
  let currentWallet = ref()

  storageWallets.value.forEach((storageWallet) => {
    if (storageWallet.pubkey === _pubkey)
      currentWallet = storageWallet
  })
  return currentWallet
}

/**
 * Creates and sends transaction of change or regular transaction
 * @param  {Array} _currentBlock array with current block data
 * @param  {Array} _utxos    utxos needed for the transaction
 * @param  {String} _amount   amount to send to receiver via the transaction
 * @param  {Wallet} _wallet   sender's wallet
 * @param  {String} _destPubkey  receiver's public key
 * @param  {String} _restPubkey  sender's public key
 * @param  {String} _comment   comment to send to receiver via the transaction
 * @return {Array}         new utxo created from transaction of change or null if no transaction of change
 */
async function createAndSendTransaction(
  _currentBlock,
  _utxos,
  _amount,
  _wallet,
  _destPubkey,
  _restPubkey,
  _comment,
) {
  console.log('utxo', _utxos)
  if (_utxos.length > MAX_INPUT_LENGTH) {
    // if utxos length > MAX (=40) then transaction of change created
    const firstUtxos = slice_utxo(_utxos, 0, MAX_INPUT_LENGTH)
    const totalAmount = getTotalUtxoAmount(firstUtxos)

    return createAndSendTransaction(
      _currentBlock,
      firstUtxos,
      totalAmount,
      _wallet,
      _wallet.pubkey,
      _wallet.pubkey,
      undefined,
    ).then((value) => {
      _utxos = removeUtxosFromArray(firstUtxos, _utxos)
      _utxos.push(value)
      console.log('utxo2', _utxos)
      return createAndSendTransaction(
        _currentBlock,
        _utxos,
        _amount,
        _wallet,
        _destPubkey,
        _restPubkey,
        _comment,
      )
    })
  }
  else {
    const blockValue = getCurrentBlock(_currentBlock)
    const transaction_data = setTransactionDocument(
      10,
      'Transaction',
      blockValue.currency,
      0,
      `${blockValue.number}-${blockValue.hash}`,
      getDataUnlocks(_utxos.length),
      _restPubkey,
      getDataOutputs(_utxos, _amount, blockValue.unitbase, _destPubkey, _restPubkey),
      getDataInputs(_utxos),
      _comment,
    )

    const signedTransaction = await signTransactionDocument(_restPubkey, transaction_data)
    // return signTransactionDocument(_restPubkey, transaction_data).then(
    //   (signedTransaction) => {
    // console.log(`\n-->valeur du document signé : \n${signedTransaction}`)

    // if this is a transaction of change then new utxo is created with generated hash
    let newUtxo
    if (_destPubkey === _restPubkey) {
      const createdHash = g1lib.crypto.sha256(signedTransaction).toString().toUpperCase()
      newUtxo = createNewUtxo('T', 0, createdHash, _amount, 0, `SIG(${_restPubkey})`)
    }

    await sendDocumentToDuniter('/tx/process', signedTransaction, duniterUrl.value)
    storeUsedUtxo(_utxos)

    return newUtxo
    //   },
    //   (error) => {
    // detailsMsg2.value += `--> Erreur du document signé : ${error}`
    // return false
    //   },
    // )
  }
}

/**
 * Gets current block data
 * @param  {Array} _response	fetched block
 * @return {Array} 				some block data (currency, hash, number, unitbase, version)
 */
function getCurrentBlock(_response) {
  const block_info = {
    currency: _response.currency,
    hash: _response.hash,
    number: _response.number,
    unitbase: _response.unitbase,
    version: _response.version,
  }
  return block_info
}

/**
 * Creates unlocks data
 * @param  {Array} _arrayLength	unlocks length
 * @return {String} 			unlocks created
 */
function getDataUnlocks(_arrayLength) {
  let unlocks_data = ''

  for (let i = 0; i < _arrayLength; i++)
    unlocks_data += `${i}:SIG(0)\n`

  return unlocks_data
}

/**
   * Creates inputs data from utxos data
   * @param  {Array} _arrayUtxo	utxos array
   * @return {String} 			inputs created
   */
function getDataInputs(_arrayUtxo) {
  let input_string = ''
  _arrayUtxo.forEach((_utxo) => {
    input_string += `${_utxo.amount}:${_utxo.base}:${_utxo.type}:${_utxo.identifier}:${_utxo.noffset}\n`
  })
  return input_string
}

/**
 * Creates outputs data
 * @param  {Array} _arrayUtxo			utxos array
 * @param  {Integer} _amount			transaction amount
 * @param  {Integer} _unitBase			outputs' unitbase
 * @param  {String} _pubkeyReceiver	receiver's public key
 * @param  {String} _pubkeySender	sender's public key
 * @return {String} 					outputs created
 */
function getDataOutputs(
  _arrayUtxo,
  _amount,
  _unitBase,
  _pubkeyReceiver,
  _pubkeySender,
) {
  const total = getTotalUtxoAmount(_arrayUtxo)
  const rest = total - _amount
  let output_string = ''
  if (rest >= 0) {
    output_string += `${_amount}:${_unitBase}:SIG(${_pubkeyReceiver})\n`
    if (rest > 0)
      output_string += `${rest}:${_unitBase}:SIG(${_pubkeySender})\n`
  }
  return output_string
}

/**
 * Adds signature in transaction data or transaction document
 * @param  {String} _pubkeySender 	sender's public key
 * @param  {String} _tx_data 			transaction data or document needed for transaction
 * @return {String}      				transaction data with signature added or signed document
 */
async function signTransactionDocument(_pubkeySender, _tx_data) {
  const currentWallet = getCurrentWallet(_pubkeySender)

  let signedDocument = await g1lib.crypto.signDocument(
    _tx_data,
    currentWallet.secretKey,
  )
  signedDocument += '\n'

  // await sleep(2000)
  return signedDocument
}

/**
 * Creates transaction data or document
 * @param  {Integer} _version	document version
 * @param  {String} _type 		document type
 * @param  {String} _currency 	document currency
 * @param  {Integer} _locktime 	document locktime
 * @param  {String} _blockstamp document blockstamp
 * @param  {String} _unlocks 	document unlocks
 * @param  {String} _issuers  	document issuers
 * @param  {String} _outputs 	document outputs
 * @param  {String} _inputs 	document inputs
 * @param  {String} _comment 	document comment
 * @return {String}      		unsigned document
 */
function setTransactionDocument(
  _version,
  _type,
  _currency,
  _locktime,
  _blockstamp,
  _unlocks,
  _issuers,
  _outputs,
  _inputs,
  _comment,
) {
  const tx_data
    = `Version: ${_version}\n`
    + `Type: ${_type}\n`
    + `Currency: ${_currency}\n`
    + `Blockstamp: ${_blockstamp}\n`
    + `Locktime: ${_locktime}\n`
    + `Issuers:\n${_issuers}\n`
    + `Inputs:\n${_inputs}Unlocks:\n${_unlocks}Outputs:\n${_outputs}Comment: ${_comment || '.'
    }\n`

  return tx_data
}

/**
 * Sends signed document to duniter
 * @param  {String} _cmd 	command to complete url (see "fetchResponse" for example)
 * @param  {String} _url 	duniter url (see "fetchResponse" for example)
 * @return {Boolean}      	says if server received valid document or not
 */
async function sendDocumentToDuniter(_cmd: string, _doc_data: any, _url: string) {
  const url = _url + _cmd

  const data = {
    transaction: _doc_data,
  }

  const fetchData = {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
    },
    // mode: 'cors',
    // cache: 'default',
    body: JSON.stringify(data),
  }

  const response = await fetch(url, fetchData)
    .then((response) => {
      return `--> OK Transaction lancée ! Server retourne un objet : ${response.statusText}`
    })
    .catch((error) => {
      return `--> Noooooo! Erreur : ${error}`
    })
  return response
}
