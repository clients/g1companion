export const duniterUrlList = [
  'https://g1.duniter.org',
  'https://duniter.adn.life',
  'https://vit.fdn.org:10900',
]
export const cesiumPlusUrlList = [
  'https://g1.data.e-is.pro',
  'https://g1.data.presles.fr',
  'https://g1.data.le-sou.org',
]

export default {
  userLocale: 'fr',
  primaryColor: '175.34, 77%, 26%',

  duniterUrl: 'https://g1.duniter.org',
  cesiumPlusUrl: 'https://g1.data.e-is.pro',

  // contentScripts
  userInfo: true,
  payButton: true,
  loginButton: true,
  fabButton: true,
}
