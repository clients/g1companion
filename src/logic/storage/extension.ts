import type { RemovableRef } from '@vueuse/core'
import { useStorageGlobal, useStorageLocal } from '~/composables/useStorage'
import defaultValues from '~/logic/storage'

export const duniterUrl = useStorageLocal('duniterUrl', defaultValues.duniterUrl, {
  listenToStorageChanges: true,
})

export const cesiumPlusUrl = useStorageLocal('cesiumPlusUrl', defaultValues.cesiumPlusUrl, {
  listenToStorageChanges: true,
})

export const storageUtxos = useStorageLocal('utxos', [], {
  listenToStorageChanges: true,
})

export const storageWallets = useStorageLocal('wallets', [] as Wallet[], {
  listenToStorageChanges: true,
})

export const currentWallet: RemovableRef<Wallet> = useStorageGlobal('currentWallet', {} as Wallet, {
  listenToStorageChanges: true,
})

export const userLocale = useStorageLocal('userLocale', defaultValues.userLocale, {
  listenToStorageChanges: true,
})

export const storageIdentities = useStorageLocal('identities', [], {
  listenToStorageChanges: true,
})

/**
 * contentScripts
 */
export const userInfo = useStorageLocal('userInfo', defaultValues.userInfo, {
  listenToStorageChanges: true,
})
export const payButton = useStorageLocal('payButton', defaultValues.payButton, {
  listenToStorageChanges: true,
})
export const loginButton = useStorageLocal('loginButton', defaultValues.loginButton, {
  listenToStorageChanges: true,
})
export const fabButton = useStorageLocal('fabButton', defaultValues.fabButton, {
  listenToStorageChanges: true,
})

export default {
  duniterUrl,
  cesiumPlusUrl,
  storageUtxos,
  storageWallets,
  currentWallet,
  storageIdentities,
}
