import { useStorage } from '@vueuse/core'
import defaultValues from '~/logic/storage'

// App specific
export const isGoBack = useStorage('isGoBack', false)
export const currentSearch = useStorage('currentSearch', false)

// User settings
export const primaryColor = useStorage('primaryColor', defaultValues.primaryColor)
