import { isRef, onMounted, ref, unref, watch } from 'vue'
import type { MaybeRef } from '@vueuse/core'
import { crypto } from 'g1lib'

import { cesiumPlusUrl, currentWallet } from '~/logic/storage/extension'

/* EXAMPLE

import { useCplusSearch, fetchCplusSearch } from '~/composables/useCplusSearch';
import { useRoute } from 'vue-router';

const route = useRoute();

// Fetched only once on mount
const data = useCplusSearch(route.params.pubkey);

// In sync whenever the param changes
const data = useCplusSearch(
  computed(() => route.params.pubkey)
);

// Or use as a fetch library
const data = await fetchCplusSearch(pubkey)
console.log(data.value)
*/

async function fetchCplus(url: string, options: {} = {}) {
  console.log('fetchCplus:', url)
  const response = fetch(url, options)
    .then(res => res.json())

  return response
}

/**
 * Search by pubkey
 */

// TODO: Use this url instead ? https://g1.data.e-is.pro/user,page,group/profile,record/_search
export async function fetchCplusSearch(pubkey: MaybeRef<string> = currentWallet.value.pubkey) {
  const data = await fetchCplus(`${cesiumPlusUrl.value}/user/profile/_search?q=issuer:${unref(pubkey)}`)
  return data?.hits?.hits[0]?._source
}

export function useCplusSearch(pubkey: MaybeRef<string>) {
  const response = ref()

  onMounted(async () => {
    response.value = await fetchCplusSearch(unref(pubkey))
  })

  if (isRef(pubkey)) {
    watch(pubkey, async (newPubkey) => {
      response.value = await fetchCplusSearch(newPubkey)
    })
  }

  return response
}

/**
 * Notifications
 */

export async function fetchCplusNotifs(pubkey: MaybeRef<string> = currentWallet.value.pubkey) {
  let options = {
    method: 'POST',
    body: JSON.stringify({
      query: {
        bool: {
          must: [{
            term: { recipient: pubkey },
          }],
          must_not: { terms: { code: ['MESSAGE_RECEIVED', 'INVITATION_TO_CERTIFY'] } },
        },
      },
      sort: [{ time: { order: 'desc' } }],
      from: 0,
      size: 40,
      _source: ['type', 'code', 'params', 'reference', 'recipient', 'time', 'hash', 'read_signature'],
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const { hits } = await fetchCplus(
    `${cesiumPlusUrl.value}/user/event/_search`,
    options,
  )
  if (!hits?.hits)
    return []

  interface User {
    _id: string
    _source: {
      time: number
      hash: string
      params: string[]
    }
  }

  // Get more info of users
  options = {
    method: 'POST',
    body: JSON.stringify({
      query: {
        constant_score: {
          filter: { terms: { _id: [...new Set(hits.hits.map((item: User) => item._source.params[0]))] } },
        },
      },
      from: 0,
      size: 40,
      _source: ['title'],
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const users = await fetchCplus(
    `${cesiumPlusUrl.value}/user/profile/_search`,
    options,
  )

  // Ouf ! We return data...
  return hits.hits
    // Remove doublons
    .filter((value: User, index: number, self: User[]) =>
      index === self.findIndex(t => (
        t._source.time === value._source.time && t._source.hash === value._source.hash
      )),
    )
    // Transform data
    .map((notif: { _source: { params: string[]; time: number }; _id: string }) => {
      const title = users.hits.hits.find((user: User) => user._id === notif._source.params[0])

      return {
        ...notif._source,
        id: notif._id,
        pubkey: notif._source.params[0],
        title: title?._source.title || notif._source.params[0],
        time: notif._source.time * 1000,
      }
    })
}

export function useCplusNotifs(pubkey?: MaybeRef<string>) {
  const response = ref()

  onMounted(async () => {
    response.value = await fetchCplusNotifs(unref(pubkey))
  })

  if (isRef(pubkey)) {
    watch(pubkey, async (newPubkey) => {
      response.value = await fetchCplusNotifs(newPubkey)
    })
  }

  return response
}

/**
 * Messages
 */

interface Message {
  title: string
  content: string
  issuer: string
}

export async function fetchCplusMessages(pubkey: MaybeRef<string> = currentWallet.value.pubkey) {
  const options = {
    method: 'POST',
    body: JSON.stringify({
      sort: { time: 'desc' },
      from: 0,
      size: 10,
      _source: ['issuer', 'recipient', 'title', 'content', 'time', 'nonce', 'read_signature'],
      query: { bool: { filter: { term: { recipient: pubkey } } } },
    }),
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const { hits } = await fetchCplus(
    `${cesiumPlusUrl.value}/message/inbox/_search`,
    options,
  )
  const data = !hits?.hits
    ? []
    : hits.hits.map((message: { _source: { time: number }; _id: string }) => {
      const decryptedMessage = crypto.textDecrypt(message._source, currentWallet.value.secretKey)
      return {
        ...message._source,
        ...decryptedMessage,
        id: message._id,
        time: message._source.time * 1000,
      }
    })

  return data as Message[]
}

export function useCplusMessages(pubkey?: MaybeRef<string>) {
  const response = ref()

  onMounted(async () => {
    response.value = await fetchCplusMessages(unref(pubkey))
  })

  if (isRef(pubkey)) {
    watch(pubkey, async (newPubkey) => {
      response.value = await fetchCplusMessages(newPubkey)
    })
  }

  return response
}
