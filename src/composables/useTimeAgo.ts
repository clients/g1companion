import { useTimeAgo as vueUseTimeAgo } from '@vueuse/core'
import type { MaybeComputedRef, UseTimeAgoOptions } from '@vueuse/core'
import type { Ref } from 'vue'

import { t } from '~/logic/i18n'

export function useTimeAgo(time: MaybeComputedRef<Date | number | string>, options: UseTimeAgoOptions<any> = {}): Ref<string> {
  const timeAgo = vueUseTimeAgo(time, {
    ...options,
    updateInterval: 10_000,
    max: 'month',
    fullDateFormatter: (date: Date) => date.toLocaleDateString(),
    messages: {
      justNow: t('just now'),
      past: n => n.match(/\d/) ? t('ago', [n]) : n,
      future: n => n.match(/\d/) ? t('in', [n]) : n,
      month: (n, past) => n === 1
        ? past
          ? t('last month')
          : t('next month')
        : `${n} ${t(`month${n > 1 ? 's' : ''}`)}`,
      year: (n, past) => n === 1
        ? past
          ? t('last year')
          : t('next year')
        : `${n} ${t(`year${n > 1 ? 's' : ''}`)}`,
      day: (n, past) => n === 1
        ? past
          ? t('yesterday')
          : t('tomorrow')
        : `${n} ${t(`day${n > 1 ? 's' : ''}`)}`,
      week: (n, past) => n === 1
        ? past
          ? t('last week')
          : t('next week')
        : `${n} ${t(`week${n > 1 ? 's' : ''}`)}`,
      hour: n => `${n} ${t(`hour${n > 1 ? 's' : ''}`)}`,
      minute: n => `${n} ${t(`minute${n > 1 ? 's' : ''}`)}`,
      second: n => `${n} ${t(`second${n > 1 ? 's' : ''}`)}`,
    },
  })

  return timeAgo
}
