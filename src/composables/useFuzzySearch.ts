import fuzzysort from 'fuzzysort'
import { unref } from 'vue'
import type { MaybeRef } from '@vueuse/core'

export function useFuzzySearch<T>(
  search: MaybeRef<string>,
  targets: ReadonlyArray<T | undefined>,
  options,
  highlight?: string,
) {
  // raw search value
  const searchValue = unref<string | MaybeRef<string>>(search) as string

  const results = fuzzysort.go(searchValue, targets, options)

  return results.map(result => ({
    ...result.obj,
    // [highlight || 'text']: fuzzysort.highlight(result),
    score: Math.abs(result.score),
  }))
}
