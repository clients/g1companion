import { storage } from 'webextension-polyfill'
import { onMessage, sendMessage } from 'webext-bridge'

import type {
  MaybeRef,
  RemovableRef,
  StorageLikeAsync,
  UseStorageAsyncOptions,
} from '@vueuse/core'

import {
  useStorageAsync,
} from '@vueuse/core'

const storageLocal: StorageLikeAsync = {
  removeItem(key: string) {
    return storage.local.remove(key)
  },

  setItem(key: string, value: string) {
    return storage.local.set({ [key]: value })
  },

  async getItem(key: string) {
    return (await storage.local.get(key))[key]
  },
}

export const useStorageLocal = <T>(
  key: string,
  initialValue: MaybeRef<T>,
  options?: UseStorageAsyncOptions<T>,
): RemovableRef<T> => useStorageAsync(key, initialValue, storageLocal, options)

const storageGlobal: StorageLikeAsync = {
  removeItem(key: string) {
    return storage.local.remove(key)
  },

  async setItem(key: string, value: string) {
    await sendMessage('change-storage', { key, value: toRaw(value) }, 'background')
    // await sendMessage('change-storage', { key, value: toRaw(value) }, 'options')
    // await sendMessage('change-storage', { key, value: toRaw(value) }, 'content-script')
    return storage.local.set({ [key]: value })
  },

  async getItem(key: string) {
    return (await storage.local.get(key))[key]
  },
}

// TODO: wrap this function to add parameters for destination : background or content-script
export const useStorageGlobal = <T>(
  key: string,
  initialValue: MaybeRef<T>,
  options?: UseStorageAsyncOptions<T>,
): RemovableRef<T> => useStorageAsync(key, initialValue, storageGlobal, options)

export const useOnStorageChange = (storage) => {
  // Subscribe to global storage change
  onMessage('change-storage', ({ data, sender }) => {
    if (data) {
      // TODO: fix typescript
      const { key, value } = data
      if (storage[key]) {
        const formatedValue = JSON.parse(value)
        storage[key].value = formatedValue
        // console.log(data, key, formatedValue)
      }
    }
  })
}
